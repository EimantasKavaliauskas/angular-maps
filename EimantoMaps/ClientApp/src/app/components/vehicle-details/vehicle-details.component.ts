import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-vehicle-details',
  templateUrl: './vehicle-details.component.html',
  styleUrls: ['./vehicle-details.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class VehicleDetailsComponent implements OnInit {

  carNumber = '';
  helpMessage = 'Norėdami gauti pagalbą kelyje, įveskite savo automobilio valstybinį numerį, spustelkite mygtuką ' +
    '"Toliau" ir sekite tolimesnes instrukcijas';

  constructor(private router: Router,
              private snackbar: MatSnackBar) { }

  ngOnInit() {
  }

  onNextClick() {
    if (this.carNumber === '') {
      this.snackbar.open('Įveskite automobilio valstybinį numerį', '', {duration: 2500});
      return;
    }
    this.router.navigate(['/set-location']);
  }

  onChange(event) {
    if (this.carNumber.length > 9) {
      event.preventDefault();
    }
  }

  onHelpClick() {
    this.snackbar.open(this.helpMessage, '', {duration: 5000});
  }
}
