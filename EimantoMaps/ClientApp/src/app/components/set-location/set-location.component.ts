import {AfterViewInit, Component, OnInit} from '@angular/core';
import {MapService} from '../../services/map.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-set-location',
  templateUrl: './set-location.component.html',
  styleUrls: ['./set-location.component.css']
})
export class SetLocationComponent implements OnInit, AfterViewInit {

  latitude = 54.9;
  longitude = 23.9;

  constructor(private mapService: MapService,
              private router: Router) { }

  ngOnInit() {
    this.mapService.getPreliminaryLocation().subscribe((result: any) => {
      this.latitude = result.latitude;
      this.longitude = result.longitude;
    }, er => {
      console.log('Preliminary location request blocked by adblocker');
    });
  }

  ngAfterViewInit() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(result => {
        this.latitude = result.coords.latitude;
        this.longitude = result.coords.longitude;
      });
    }
  }

  onDrop(event) {
    this.longitude = event.coords.lng;
    this.latitude = event.coords.lat;
  }

  onNextClick() {
    this.router.navigate(['/assistance-tracker'], {queryParams: {latitude: this.latitude, longitude: this.longitude}});
  }
}
