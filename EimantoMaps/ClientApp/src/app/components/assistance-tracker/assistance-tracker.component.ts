/* tslint:disable:max-line-length */
import {AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AgmMap, LatLngBounds} from '@agm/core';
import {del} from 'selenium-webdriver/http';

declare var google: any;

@Component({
  selector: 'app-assistance-tracker',
  templateUrl: './assistance-tracker.component.html',
  styleUrls: ['./assistance-tracker.component.css']
})
export class AssistanceTrackerComponent implements OnInit, AfterViewInit, OnDestroy{

  @ViewChild('agmMap') agmMap: AgmMap;
  componentActive = true;
  timeToArrivalLabel = '00:00';
  timeToArrival = 0;
  routeVisible = true;
  latitude = 58.9;
  longitude = 23.9;
  assistanceLatitude = 54.9;
  assistanceLongitude = 23.9;
  waypoints = [];
  routesFetched = false;
  carIconUrl = '../../../assets/car.svg';

  constructor(private route: ActivatedRoute,
              private router: Router,
              private changeDetector: ChangeDetectorRef) {
      route.queryParams.subscribe(params => {
      this.latitude = params.latitude - 0;
      this.longitude = params.longitude - 0;
    });
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
  }

  onNextClick() {
    this.fitMapToBounds();
  }

  calculateDelay(waypoint1, waypoint2) {
    const distance = Math.sqrt(Math.pow(waypoint1.latitude - waypoint2.latitude, 2)
      + Math.pow(waypoint1.longitude - waypoint2.longitude, 2));
    return (distance * 4000000) + 1500;
  }

  plotNextWaypoint() {
    if (this.waypoints.length === 0 || !this.componentActive) {
      return;
    } else {
      const delay = this.calculateDelay(this.waypoints[0], {longitude: this.assistanceLongitude, latitude: this.assistanceLatitude});
      setTimeout(() => {
        if (!this.componentActive) {
          return;
        }
        this.assistanceLongitude = this.waypoints[0].longitude;
        this.assistanceLatitude = this.waypoints[0].latitude;
        this.waypoints.splice(0, 1);
        this.fitMapToBounds();
        this.plotNextWaypoint();
      }, delay);
    }
  }

  changeRoute(routeNo) {
    routeNo === 1 ? this.router.navigate(['/']) : this.router.navigate(['/set-location']);
  }

  startJourney() {
    this.assistanceLongitude = this.waypoints[0].longitude;
    this.assistanceLatitude = this.waypoints[0].latitude;
    this.waypoints.splice(0, 1);
    this.fitMapToBounds();
    setTimeout(() => this.plotNextWaypoint(), 3000);
  }

  fitMapToBounds() {
    const bounds: LatLngBounds = new google.maps.LatLngBounds();
    bounds.extend(new google.maps.LatLng(this.latitude, this.longitude));
    bounds.extend(new google.maps.LatLng(this.assistanceLatitude, this.assistanceLongitude));
    this.agmMap.fitBounds = bounds;
    this.agmMap.triggerResize();
  }

  setTimeToArrivalLabel() {
    for (let i = 1; i < this.waypoints.length; i++) {
      this.timeToArrival += this.calculateDelay(this.waypoints[i - 1], this.waypoints[i]);
    }
    this.generateLabel();
    this.tickCountdown();
  }

  async generateLabel() {
    const minutes = Math.trunc(this.timeToArrival / 60000) > 9 ? Math.trunc(this.timeToArrival / 60000) : '0' + Math.trunc(this.timeToArrival / 60000);
    const seconds = Math.trunc((this.timeToArrival % 60000) / 1000) > 9 ? Math.trunc((this.timeToArrival % 60000) / 1000) : '0' + Math.trunc((this.timeToArrival % 60000) / 1000);
    const label = minutes + ':' + seconds;
    this.timeToArrivalLabel = label;
  }

  tickCountdown() {
    if (this.timeToArrival > 0 && this.componentActive) {
      setTimeout(() => {
        if (!this.componentActive) {
          return;
        }
        this.timeToArrival -= 1000;
        this.generateLabel();
        this.changeDetector.detectChanges();
        this.tickCountdown();
      }, 1000);
    } else {
      this.timeToArrivalLabel = '00:00';
    }
  }

  change(event) {
    if (!this.routesFetched) {
      this.routesFetched = true;
      this.routeVisible = false;
      event.routes[0].overview_path.forEach(waypoint => {
        const lat = waypoint.lat();
        const lng = waypoint.lng();
        this.waypoints.push({latitude: lat, longitude: lng});
      });
      this.setTimeToArrivalLabel();
      this.startJourney();
    }
  }

  ngOnDestroy() {
    this.componentActive = false;
  }
}
