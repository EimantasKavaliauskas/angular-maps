import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';


import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatSnackBarModule } from '@angular/material';
import { VehicleDetailsComponent } from './components/vehicle-details/vehicle-details.component';
import { SetLocationComponent } from './components/set-location/set-location.component';
import { AssistanceTrackerComponent } from './components/assistance-tracker/assistance-tracker.component';

@NgModule({
  declarations: [
    AppComponent,
    VehicleDetailsComponent,
    SetLocationComponent,
    AssistanceTrackerComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: VehicleDetailsComponent, pathMatch: 'full' },
      { path: 'set-location', component: SetLocationComponent },
      { path: 'assistance-tracker', component: AssistanceTrackerComponent },
    ]),
    BrowserAnimationsModule,
    MatButtonModule,
    MatSnackBarModule,
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyANASxsV_Pk9mBBc_8Y2YX-DXGcFLPrnJw'
    }),
    AgmDirectionModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
